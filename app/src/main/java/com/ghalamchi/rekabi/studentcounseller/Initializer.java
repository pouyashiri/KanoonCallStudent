package com.ghalamchi.rekabi.studentcounseller;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ghalamchi.rekabi.studentcounseller.R;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class Initializer extends Application {
    private RequestQueue mRequestQueue;
    private static Initializer mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());


        mInstance = this;
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/B Traffic Bold_p30download.com.ttf")
                .setFontAttrId(R.attr.fontPath)
                .disableCustomViewInflation()
                .disablePrivateFactoryInjection()
                .build()
        );
    }

    public static synchronized Initializer getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {

            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

}
