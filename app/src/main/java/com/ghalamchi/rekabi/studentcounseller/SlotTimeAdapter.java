package com.ghalamchi.rekabi.studentcounseller;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ghalamchi.rekabi.studentcounseller.R;

import java.util.ArrayList;

public class SlotTimeAdapter extends BaseAdapter {
    Context context;
    ArrayList<String> SlotTime;
    LayoutInflater inflater;

    public SlotTimeAdapter(Context context,ArrayList<String> SlotTime){
        this.context = context;
        this.SlotTime = SlotTime;
        inflater = LayoutInflater.from(context);
    }




    @Override
    public int getCount() {
        return SlotTime.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if(convertView==null){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_slottime,parent,false);
            viewHolder.la_SlotTime = convertView.findViewById(R.id.la_slotTime);
            viewHolder.txt_SlotTime = convertView.findViewById(R.id.txt_slotTime);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.txt_SlotTime.setTag(position);
        viewHolder.la_SlotTime.setTag(position);
        viewHolder.txt_SlotTime.setText(SlotTime.get(position));
        final Typeface tvFont = Typeface.createFromAsset(context.getAssets(), "fonts/B Traffic Bold_p30download.com.ttf");
        viewHolder.txt_SlotTime.setTypeface(tvFont);
        return convertView;
    }

    public class ViewHolder{
        LinearLayout la_SlotTime;
        TextView txt_SlotTime;
    }
}
