package com.ghalamchi.rekabi.studentcounseller;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.fabric.sdk.android.Fabric;

public class SupporterInfoActivity extends AppCompatActivity {

    String userId,userName;
    private TextView new_count;
    String reqId;
    ImageView supImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supporter_info);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Fabric.with(this, new Crashlytics());
        logUser();

        Button exit,CallListPage,BookListPage,HomePage,SupPage;
        exit = findViewById(R.id.exit);
        CallListPage = findViewById(R.id.CallListPage);
        BookListPage = findViewById(R.id.BookListPage);
        HomePage = findViewById(R.id.HomePage);
        SupPage = findViewById(R.id.SupPage);
        new_count = findViewById(R.id.new_count);
        new_count.setVisibility(View.INVISIBLE);
        supImage = findViewById(R.id.supImage);

        String supFullName,supField,supUniversity,supExamRank;
        TextView supporterName,supporterField,supporterUni,supporterExamrank;

        supporterName = findViewById(R.id.supporterName);
        supporterField = findViewById(R.id.supporterField);
        supporterUni = findViewById(R.id.supporterUni);
        supporterExamrank = findViewById(R.id.supporterExamrank);


        SharedPreferences settings = getSharedPreferences("UserId",0);
        SharedPreferences.Editor editor = settings.edit();
        userId = settings.getString("userId","");
        userName = settings.getString("userName","");
        reqId = settings.getString("reqId","");

        if (settings.contains("supFullName")){
            supFullName = settings.getString("supFullName","" );
            supField = settings.getString("supField","" );
            supUniversity = settings.getString("supUniversity","" );
            supExamRank = settings.getString("supExamRank","" );

            ////////////set supporter image here :
            supImage.setImageDrawable(getResources().getDrawable(R.drawable.supp2));

            supporterName.setText(supFullName);
            supporterField.setText("رشته: "+supField);
            supporterUni.setText("دانشگاه: "+supUniversity);
            supporterExamrank.setText("رتبه کنکور: "+supExamRank);
        }

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Alertdialog_exit();
            }
        });

        BookListPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SupporterInfoActivity.this, BookListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("userId", userId);
                intent.putExtra("userName", userName);
                startActivity(intent);
            }
        });

        CallListPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SupporterInfoActivity.this, CallListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("userId", userId);
                intent.putExtra("userName", userName);
                startActivity(intent);
            }
        });

        HomePage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SupporterInfoActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("userId", userId);
                intent.putExtra("userName", userName);
                startActivity(intent);
            }
        });

        SupPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // // Im Here
            }
        });










    }

    @Override
    protected void onResume() {
        GetNewRecomState(userId);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                GetCurrentRequestState(reqId,"0");
            }
        },1000);

        super.onResume();
    }

    public void GetCurrentRequestState(final String reqId, final String remove) {
        SpotsDialog alertDialog = new SpotsDialog(SupporterInfoActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        Map<String, String> CurrentReqParams = new HashMap<String, String>();
        CurrentReqParams.put("reqId", reqId);
        final String CurrentReqUrl = getResources().getString(R.string.url) + "GetCurrentRequestState";
        new VolleyPost(SupporterInfoActivity.this, CurrentReqUrl, CurrentReqParams, 1000, null, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("CurrentReqState", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    if (success.equals("1")) {
                        String DoneState = jsonObject.getString("DoneState");

                        if (DoneState.equals("1")) {
                            Log.e("DoneState==", DoneState);
                            JSONObject supporter = jsonObject.getJSONObject("supporter");
                            String supporterName = supporter.getString("FullName");
                            Intent intent = new Intent(SupporterInfoActivity.this, FeedbackActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("supporterName", supporterName);
                            intent.putExtra("reqId", reqId);
                            startActivity(intent);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void Alertdialog_exit() {
        final Dialog My_alertdialog = new Dialog(SupporterInfoActivity.this);
        My_alertdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        My_alertdialog.setContentView(R.layout.alert_style_exit);
        Button yesExit = (Button) My_alertdialog.findViewById(R.id.yesExit);
        yesExit.setEnabled(true);
        Button noExit = (Button) My_alertdialog.findViewById(R.id.noExit);
        noExit.setEnabled(true);

        yesExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences settings = getSharedPreferences("UserId", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.clear();
                editor.commit();
                Intent intent = new Intent(SupporterInfoActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                My_alertdialog.cancel();
            }
        });
        noExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                My_alertdialog.cancel();
            }
        });
        My_alertdialog.setCancelable(true);
        My_alertdialog.show();
    }

    public void GetNewRecomState(String userId){
        SpotsDialog alertDialog = new SpotsDialog(SupporterInfoActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> RemoveRequestParams = new HashMap<String, String>();
        RemoveRequestParams.put("userId", userId);

        final String RemoveRequestUrl = getResources().getString(R.string.url) + "GetNewRecomState";
        new VolleyPost(SupporterInfoActivity.this, RemoveRequestUrl, RemoveRequestParams, 1000, null, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("GetNewRecomState", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    int count = jsonObject.getInt("count");
                    if(count==0){
                        //// Invisible the new circle
                        new_count.setVisibility(View.INVISIBLE);
                    }else{
                        new_count.setVisibility(View.VISIBLE);
                        String countStr = String.valueOf(count);
                        new_count.setText(countStr);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SupporterInfoActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("userId", userId);
        intent.putExtra("userName", userName);
        startActivity(intent);
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SharedPreferences settings = getSharedPreferences("UserId", 0);
        Crashlytics.setUserIdentifier(settings.getString("userId", ""));
    }

}
