package com.ghalamchi.rekabi.studentcounseller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.ghalamchi.rekabi.studentcounseller.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity {

    private EditText phone;
    private Button getSMS,Register;
    private String phonenumber, login_success, userid;
    private SpotsDialog alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Fabric.with(this, new Crashlytics());
        logUser();

        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
            System.exit(0);
        }

        phone = findViewById(R.id.phone);
        getSMS = findViewById(R.id.getSMS);
        Register = findViewById(R.id.register);

        SharedPreferences settings = getSharedPreferences("UserId", getApplicationContext().MODE_PRIVATE);
        if ((settings.contains("userId"))) {
            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {

            Register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });


            getSMS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    phonenumber = phone.getText().toString();

                    alertDialog = new SpotsDialog(LoginActivity.this, R.style.Custom);
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);

                    Map<String, String> loginParams = new HashMap<String, String>();
                    loginParams.put("mobile", phonenumber);
                    final String loginUrl = getResources().getString(R.string.url)+"LoginUser";
                    new VolleyPost(LoginActivity.this, loginUrl, loginParams, 1000, alertDialog, new VolleyPost.onResponse() {
                        @Override
                        public void onFinish(String Result) {
                            try {
                                Log.e("login", Result);
                                JSONObject jsonObject = new JSONObject(Result);
                                login_success = jsonObject.getString("success");


                                if (login_success.equals("0")) {
                                    Toast.makeText(getApplicationContext(), "شماره همراه شما به ثبت نرسیده، لطفا ثبت نام کنید!", Toast.LENGTH_LONG).show();
                                } else if (login_success.equals("1")) {
                                    JSONObject user = jsonObject.getJSONObject("user");
                                    String userid = user.getString("Id");
                                    String userName = user.getString("FullName");
                                    Log.e(login_success+":::",login_success);
                                    Log.e(userid+":::",userid);
                                    SharedPreferences settings = getSharedPreferences("UserId",  0);
                                    SharedPreferences.Editor editor = settings.edit();
                                    editor.putString("registerCode", "ok");
                                    editor.putString("userName", userName);
                                    editor.putString("phone", phonenumber);
                                    editor.putString("userId", userid);
                                    editor.commit();
                                    editor.apply();

                                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra("userName", userName);
                                    intent.putExtra("phone", phonenumber);
                                    intent.putExtra("userId", userid);
                                    startActivity(intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });


                }
            });


        }
    }

    @Override
    public void onBackPressed() {
       finish();
    }


    public void ActivateSupporter(){
        alertDialog = new SpotsDialog(LoginActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> loginParams = new HashMap<String, String>();
        loginParams.put("mobile", phonenumber);
        final String loginUrl = getResources().getString(R.string.url)+"LoginUser";
        new VolleyPost(LoginActivity.this, loginUrl, loginParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("login", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    login_success = jsonObject.getString("success");
                    JSONObject user = jsonObject.getJSONObject("user");
                    String userid = user.getString("Id");
                    String userName = user.getString("FullName");

                    Log.e(login_success+":::",login_success);
                    Log.e(userid+":::",userid);

                    if (login_success.equals("0")) {
                        Toast.makeText(getApplicationContext(), "شماره همراه شما به ثبت نرسیده، لطفا ثبت نام کنید!", Toast.LENGTH_LONG).show();
                    } else if (login_success.equals("1")) {
                        SharedPreferences settings = getSharedPreferences("UserId",  getApplicationContext().MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("userName", userName);
                        editor.putString("phone", phonenumber);
                        editor.putString("userId", userid);
                        editor.commit();
                        editor.apply();

                        Intent intent = new Intent(LoginActivity.this, SmsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("userName", userName);
                        intent.putExtra("phone", phonenumber);
                        intent.putExtra("userId", userid);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog!=null) {
            alertDialog.dismiss();
            alertDialog=null;
        }
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SharedPreferences settings = getSharedPreferences("UserId", getApplicationContext().MODE_PRIVATE);
        Crashlytics.setUserIdentifier(settings.getString("userId", ""));
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
