package com.ghalamchi.rekabi.studentcounseller;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.CallLog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.ghalamchi.rekabi.studentcounseller.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HomeActivity extends AppCompatActivity {
    private SpotsDialog alertDialog;

    private Button exit;
    private Button yesExit, noExit;
    private Dialog My_alertdialog, Slotdialog;
    private int prev_state;
    private TextView userNametext, new_count;
    private SharedPreferences preferences;

    private int turn = 1;
    private int flag = 0;
    private Button CallListPage, BookListPage, HomePage, SupPage;
    private String userId, FeedbackDone, userName;
    private LinearLayout La_removeReq, La_createReq;
    private TextView slotTimeTxt;
    private Button removeRequest, createRequest;
    private String sloTime, supporterName, DoneState, IsRemove, ReserveDate;

    private ArrayList<String> StringDate = new ArrayList<>();
    private ArrayList<String> Slot = new ArrayList<>();
    private ArrayList<String> LongDate = new ArrayList<>();

    private String reqId, setlongDate, setSlot, setDate, DoneRequest = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Fabric.with(this, new Crashlytics());
        logUser();
        Log.e("------", "--------OnCreate---------");
        userNametext = findViewById(R.id.userName);
        exit = findViewById(R.id.exit);
        CallListPage = findViewById(R.id.CallListPage);
        BookListPage = findViewById(R.id.BookListPage);
        HomePage = findViewById(R.id.HomePage);
        SupPage = findViewById(R.id.SupPage);

        new_count = findViewById(R.id.new_count);
        new_count.setVisibility(View.INVISIBLE);

        La_removeReq = findViewById(R.id.la_removeReq);
        La_createReq = findViewById(R.id.la_createReq);
        removeRequest = findViewById(R.id.removeRequest);
        createRequest = findViewById(R.id.createRequest);
        slotTimeTxt = findViewById(R.id.slotTimeTxt);

        La_createReq.setVisibility(View.VISIBLE);
        La_removeReq.setVisibility(View.INVISIBLE);
        new_count.setVisibility(View.INVISIBLE);

        SharedPreferences preferences = getSharedPreferences("UserId", 0);
//        Bundle data = getIntent().getExtras();

        userId = getIntent().getStringExtra("userId");

        if (userId != null) {
            userName = getIntent().getStringExtra("userName");
            userNametext.setText("سلام " + userName + "!");
        } else {
            userId = preferences.getString("userId", "");
            if (preferences.contains("userName")) {
                userName = preferences.getString("userName", "");
                userNametext.setText("سلام " + userName + "!");
            } else {
                // NO web Service for name
                userNametext.setVisibility(View.INVISIBLE);
            }
        }
        Log.e("userid::::", "I am " + userId);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("userId", userId);
        editor.putString("usersss", userId);
        editor.putString("userName", userName);
        editor.commit();
        editor.apply();

    }


    @Override
    protected void onResume() {

        Log.e("on resume---------", "im here");
        La_createReq.setVisibility(View.VISIBLE);
        La_removeReq.setVisibility(View.INVISIBLE);
        new_count.setVisibility(View.INVISIBLE);

        final SharedPreferences preferences = getSharedPreferences("UserId", 0);
        userId = getIntent().getStringExtra("userId");

        if (userId != null) {
            userName = getIntent().getStringExtra("userName");
            userNametext.setText("سلام " + userName + "!");
        } else {
            userId = preferences.getString("userId", "");
//            if (preferences.contains("usersss")) {
//                String usersss = preferences.getString("usersss", "");
//                Log.e("usersss is-------","-----"+usersss);
//                Log.e("userId is-------","-----"+userId);
//            }
            if (preferences.contains("userName")) {
                userName = preferences.getString("userName", "");
                userNametext.setText("سلام " + userName + "!");
            } else {
                // NO web Service for name
                userNametext.setVisibility(View.INVISIBLE);
            }
        }
        Log.e("userid::::", "I am " + userId);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("userId", userId);
        editor.putString("userName", userName);
        editor.commit();
        editor.apply();


        SharedPreferences settings = getSharedPreferences("UserId", HomeActivity.this.MODE_PRIVATE);

        if (!settings.contains("registerCode")) {

            SharedPreferences.Editor editor1 = settings.edit();
            editor1.clear();
            editor1.commit();
            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {

            reqId = preferences.getString("reqId", "");
            setDate = preferences.getString("setDate", "");
            if (setDate != null) {
                slotTimeTxt.setText("تاریخ " + setDate + "با شما تماس گرفته می شود.");
            }

            createRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getCurrentSlotTime();
                }
            });
            removeRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    GetCurrentRequestState(reqId, "1");


                }
            });

            HomePage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Im here
                }
            });

            SupPage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences settings = getSharedPreferences("UserId", 0);
                    if (settings.contains("reqId")) {
                        String reqId = settings.getString("reqId", "");
                        if (reqId == null || reqId.equals("")) {
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("supFullName", "");
                            editor.putString("supField", "");
                            editor.putString("supUniversity", "");
                            editor.putString("supExamRank", "");
                            editor.commit();
                            editor.apply();
                            Toast.makeText(HomeActivity.this, "درخواست مشاوره ای یافت نشد.", Toast.LENGTH_LONG).show();
                        } else {
                            CheckSupporter(reqId);
                        }
                    } else {
                        Toast.makeText(HomeActivity.this, "درخواست مشاوره ای یافت نشد.", Toast.LENGTH_LONG).show();
                    }
                }
            });


            CallListPage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this, CallListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("userId", userId);
                    intent.putExtra("userName", userName);
                    startActivity(intent);
                }
            });

            BookListPage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this, BookListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("userId", userId);
                    intent.putExtra("userName", userName);
                    startActivity(intent);
                }
            });

            exit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Alertdialog_exit();
                }
            });

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    GetCurrentRequestState(reqId, "0");

                }
            }, 1000);


            GetNewRecomState(userId);
            CheckSupporterResume(reqId);

        }
        super.onResume();
//        registerReceiver(this.broadcastReceiver, new IntentFilter(TelephonyManager.ACTION_PHONE_STATE_CHANGED));

    }


//    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//
//
//            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//            HomeActivity.CustomPhoneStateListener customPhoneListener = new HomeActivity.CustomPhoneStateListener();
//            telephonyManager.listen(customPhoneListener, PhoneStateListener.LISTEN_CALL_STATE); //Register our listener with TelephonyManager
//
//        }
//    };

//
//    public class CustomPhoneStateListener extends PhoneStateListener {
//
//
//        private static final String TAG = "CustomPhoneStateListener";
//
//        @Override
//        public void onCallStateChanged(int state, String incomingNumber) {
//
//
//            if (state == TelephonyManager.CALL_STATE_RINGING) {
//                Log.e("TAG", "CALL_STATE_RINGING");
//                prev_state = state;
//            } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
//                Log.e("TAG", "CALL_STATE_OFFHOOK");
//                prev_state = state;
//            } else if (state == TelephonyManager.CALL_STATE_IDLE) {
//
//                if ((prev_state == TelephonyManager.CALL_STATE_OFFHOOK)) {
//                    prev_state = TelephonyManager.CALL_STATE_IDLE;
//                    /////////////////// CALL LOG read /////////////////
////                    Handler handler = new Handler();
////                    handler.postDelayed(new Runnable() {
////                        @Override
////                        public void run() {
//////                            GetCurrentRequestState(reqId);
//////                            if(turn==1){
//                            Log.e("telphony :","-----------Im here---------");
////                            onResume();
////                    Handler handler = new Handler();
////                    handler.postDelayed(new Runnable() {
////                        @Override
////                        public void run() {
//////                            GetCurrentRequestState(reqId);
////                        }
////                    },1000);
//
//
//////                            turn=2;
//
//////                            }else{
//////                                // todo
//////                            }
////                        }
////                    },1000);
//
//
//                }
//                if ((prev_state == TelephonyManager.CALL_STATE_RINGING)) {
//                    prev_state = state;
//                    //Rejected or Missed call
////                    Handler handler = new Handler();
////                    handler.postDelayed(new Runnable() {
////                        @Override
////                        public void run() {
//////                            GetCurrentRequestState(reqId);
////                            onResume();
////                        }
////                    },1000);
//                }
//
//            }
//        }
//    }
//


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("------", "----------onRestart--------------");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("------", "----------onStart--------------");
    }


    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences preferences = getSharedPreferences("UserId", 0);
        SharedPreferences.Editor editor = preferences.edit();
        Log.e("userid is-------", userId + "---");
        editor.putString("userId", userId);
        editor.putString("userName", userName);
        editor.commit();
        editor.apply();
    }

    public void GetCurrentRequestState(final String reqId, final String remove) {
        alertDialog = new SpotsDialog(HomeActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        Map<String, String> CurrentReqParams = new HashMap<String, String>();
        CurrentReqParams.put("reqId", reqId);
        final String CurrentReqUrl = getResources().getString(R.string.url) + "GetCurrentRequestState";
        new VolleyPost(HomeActivity.this, CurrentReqUrl, CurrentReqParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("CurrentReqState", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    if (success.equals("1")) {
                        DoneState = jsonObject.getString("DoneState");
                        IsRemove = jsonObject.getString("IsRemove");
                        ReserveDate = jsonObject.getString("ReserveDate");
                        slotTimeTxt.setText("تاریخ " + ReserveDate + "با شما تماس گرفته می شود.");
                        Log.e(" DoneState==", "--" + DoneState + "--");

                        if (DoneState.equals("0") && IsRemove.equals("0")) {
                            if (DoneState.equals("0") && remove.equals("1")) {
                                RemoveRequest();
                            } else {
                                Log.e("DoneState==", DoneState);
                                La_createReq.setVisibility(View.INVISIBLE);
                                La_removeReq.setVisibility(View.VISIBLE);
                                removeRequest.setVisibility(View.VISIBLE);
                                removeRequest.setEnabled(true);
                                createRequest.setVisibility(View.INVISIBLE);
                                createRequest.setEnabled(false);
                            }

                        }
                        if (DoneState.equals("0") && IsRemove.equals("1")) {
                            Log.e("IsRemove==", IsRemove);
                            La_createReq.setVisibility(View.VISIBLE);
                            La_removeReq.setVisibility(View.INVISIBLE);
                            removeRequest.setVisibility(View.INVISIBLE);
                            removeRequest.setEnabled(false);
                            createRequest.setVisibility(View.VISIBLE);
                            createRequest.setEnabled(true);
                        }
                        if (DoneState.equals("1")) {
                            Log.e("DoneState==", DoneState);
                            JSONObject supporter = jsonObject.getJSONObject("supporter");
                            supporterName = supporter.getString("FullName");
                            Intent intent = new Intent(HomeActivity.this, FeedbackActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("supporterName", supporterName);
                            intent.putExtra("reqId", reqId);
                            startActivity(intent);
                        }

                    } else if (success.equals("0")) {
                        Log.e("DoneState==", "0" + "--");
                        La_createReq.setVisibility(View.VISIBLE);
                        La_removeReq.setVisibility(View.INVISIBLE);
                        removeRequest.setVisibility(View.INVISIBLE);
                        removeRequest.setEnabled(false);
                        createRequest.setVisibility(View.VISIBLE);
                        createRequest.setEnabled(true);
                    } else if (success.equals("2")) {
                        Log.e("DoneState==", "2" + "--");
                        Toast.makeText(HomeActivity.this, "درخواست شما از طرف مشاور پذیرفته شده و تا دقایقی دیگر با شما تماس خواهد گرفت. برای مشاهده اطلاعات مشاور به قسمت \"مشاور من\" مراجعه کنید. ", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void getCurrentSlotTime() {
        StringDate.clear();
        Slot.clear();
        LongDate.clear();

        alertDialog = new SpotsDialog(HomeActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> slotParams = new HashMap<String, String>();
        slotParams.put("userId", userId);

        final String GetIncomingSlotsUrl = getResources().getString(R.string.url) + "GetIncomingSlots";
        new VolleyPost(HomeActivity.this, GetIncomingSlotsUrl, slotParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("GetIncomingSlots", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    JSONArray jsonArrayResult = jsonObject.getJSONArray("suggestions");
                    for (int i = 0; i < jsonArrayResult.length(); i++) {
                        JSONObject suggestionsObject = jsonArrayResult.getJSONObject(i);
                        StringDate.add(suggestionsObject.getString("StringDate"));
                        Slot.add(suggestionsObject.getString("Slot"));
                        LongDate.add(suggestionsObject.getString("LongDate"));
                    }
                    AlertDialog_SlotTimes();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void FinalizeReserve(final String userId, String setlongDate, String setSlot) {

        alertDialog = new SpotsDialog(HomeActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> finalizeParams = new HashMap<String, String>();
        finalizeParams.put("userId", userId);
        finalizeParams.put("LongDate", setlongDate);
        finalizeParams.put("Slot", setSlot);

        final String FinalizeReserveUrl = getResources().getString(R.string.url) + "FinalizeReserve";
        new VolleyPost(HomeActivity.this, FinalizeReserveUrl, finalizeParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("FinalizeReserve", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");

                    if (success.equals("1")) {
                        turn = 1;
                        slotTimeTxt.setText("تاریخ " + setDate + "با شما تماس گرفته می شود.");
                        La_createReq.setVisibility(View.INVISIBLE);
                        La_removeReq.setVisibility(View.VISIBLE);
                        removeRequest.setVisibility(View.VISIBLE);
                        removeRequest.setEnabled(true);
                        createRequest.setVisibility(View.INVISIBLE);
                        createRequest.setEnabled(false);
                        Slotdialog.cancel();
                        String reqId0 = jsonObject.getString("reqId");
                        reqId = reqId0;
                        DoneRequest = "0";
                        FeedbackDone = "0";
                        SharedPreferences settings = getSharedPreferences("UserId", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("reqId", reqId);
                        editor.putString("DoneRequest", DoneRequest);
                        editor.putString("FeedbackDone", FeedbackDone);
                        editor.putString("setDate", setDate);
                        editor.putString("userId", userId);
                        editor.putString("userName", userName);
                        editor.commit();
                        editor.apply();
                    } else {
                        La_createReq.setVisibility(View.VISIBLE);
                        La_removeReq.setVisibility(View.INVISIBLE);
                        removeRequest.setVisibility(View.INVISIBLE);
                        removeRequest.setEnabled(false);
                        createRequest.setVisibility(View.VISIBLE);
                        createRequest.setEnabled(true);
                        Slotdialog.cancel();
                        Toast.makeText(HomeActivity.this, "ثبت درخواست شما با مشکل مواجه شد! لطفا مجددا درخواست بدهید.", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void AlertDialog_SlotTimes() {


        Slotdialog = new Dialog(HomeActivity.this);
        Slotdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Slotdialog.setContentView(R.layout.dialog_slottime);
        ListView listViewSlotTime = Slotdialog.findViewById(R.id.SlotListView);
        SlotTimeAdapter slotTimeAdapter = new SlotTimeAdapter(Slotdialog.getContext(), StringDate);
        listViewSlotTime.setAdapter(slotTimeAdapter);

        listViewSlotTime.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("position:::", position + "");
                setDate = StringDate.get(position);
                setlongDate = LongDate.get(position);
                setSlot = Slot.get(position);
                FinalizeReserve(userId, setlongDate, setSlot);
            }
        });

        Slotdialog.setCancelable(true);
        Slotdialog.show();

    }


    public void RemoveRequest() {
        alertDialog = new SpotsDialog(HomeActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> RemoveRequestParams = new HashMap<String, String>();
        RemoveRequestParams.put("reqId", reqId);

        final String RemoveRequestUrl = getResources().getString(R.string.url) + "RemoveRequest";
        new VolleyPost(HomeActivity.this, RemoveRequestUrl, RemoveRequestParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("RemoveRequest", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    if (success.equals("1")) {
                        Toast.makeText(HomeActivity.this, "درخواست شما لغو گردید!", Toast.LENGTH_LONG).show();
                        reqId = null;
                        SharedPreferences settings = getSharedPreferences("UserId", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("reqId", reqId);
                        editor.putString("userName", userName);
                        editor.putString("userId", userId);
                        editor.commit();
                        editor.apply();
                        La_createReq.setVisibility(View.VISIBLE);
                        La_removeReq.setVisibility(View.INVISIBLE);
                        removeRequest.setVisibility(View.INVISIBLE);
                        removeRequest.setEnabled(false);
                        createRequest.setVisibility(View.VISIBLE);
                        createRequest.setEnabled(true);
                    }else if(success.equals("2")){
                        Toast.makeText(HomeActivity.this,"درخواست شما پذیرفته شده و امکان لغو آن وجود ندارد.",Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void GetNewRecomState(String userId) {
        alertDialog = new SpotsDialog(HomeActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> RemoveRequestParams = new HashMap<String, String>();
        RemoveRequestParams.put("userId", userId);

        final String RemoveRequestUrl = getResources().getString(R.string.url) + "GetNewRecomState";
        new VolleyPost(HomeActivity.this, RemoveRequestUrl, RemoveRequestParams, 1000, null, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("GetNewRecomState", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    int count = jsonObject.getInt("count");
                    if (count == 0) {
                        //// Invisible the new circle
                        new_count.setVisibility(View.INVISIBLE);
                    } else {
                        new_count.setVisibility(View.VISIBLE);
                        String countStr = String.valueOf(count);
                        new_count.setText(countStr);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void Alertdialog_exit() {
        My_alertdialog = new Dialog(HomeActivity.this);
        My_alertdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        My_alertdialog.setContentView(R.layout.alert_style_exit);
        yesExit = (Button) My_alertdialog.findViewById(R.id.yesExit);
        yesExit.setEnabled(true);
        noExit = (Button) My_alertdialog.findViewById(R.id.noExit);
        noExit.setEnabled(true);

        yesExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences settings = getSharedPreferences("UserId", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.clear();
                editor.commit();
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                My_alertdialog.cancel();
            }
        });
        noExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                My_alertdialog.cancel();
            }
        });
        My_alertdialog.setCancelable(true);
        My_alertdialog.show();
    }


    public void CheckSupporter(String reqId0) {

        alertDialog = new SpotsDialog(HomeActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> GetIsCallingStateParams = new HashMap<String, String>();
        GetIsCallingStateParams.put("reqId", reqId0);

        final String GetIsCallingStateUrl = getResources().getString(R.string.url) + "GetIsCallingState";
        new VolleyPost(HomeActivity.this, GetIsCallingStateUrl, GetIsCallingStateParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("GetIsCallingState", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    if (success.equals("1")) {
                        String Calling = jsonObject.getString("Calling");
                        if (Calling.equals("1")) {
                            JSONObject supporter = jsonObject.getJSONObject("supporter");
                            String supFullName = supporter.getString("FullName");
                            String supField = supporter.getString("Field");
                            String supUniversity = supporter.getString("University");
                            String supExamRank = supporter.getString("ExamRank");
                            SharedPreferences settings = getSharedPreferences("UserId", 0);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("supFullName", supFullName);
                            editor.putString("supField", supField);
                            editor.putString("supUniversity", supUniversity);
                            editor.putString("supExamRank", supExamRank);
                            editor.commit();
                            editor.apply();
                            Intent intent = new Intent(HomeActivity.this, SupporterInfoActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } else if (Calling.equals("0")) {
                            SharedPreferences settings = getSharedPreferences("UserId", 0);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("supFullName", "");
                            editor.putString("supField", "");
                            editor.putString("supUniversity", "");
                            editor.putString("supExamRank", "");
                            editor.commit();
                            editor.apply();
                            Toast.makeText(HomeActivity.this, "تا کنون مشاوری به درخواست شما پاسخ نداده است. لطفا منتظر بمانید!", Toast.LENGTH_LONG).show();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void CheckSupporterResume(String reqId0) {

        alertDialog = new SpotsDialog(HomeActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> GetIsCallingStateParams = new HashMap<String, String>();
        GetIsCallingStateParams.put("reqId", reqId0);

        final String GetIsCallingStateUrl = getResources().getString(R.string.url) + "GetIsCallingState";
        new VolleyPost(HomeActivity.this, GetIsCallingStateUrl, GetIsCallingStateParams, 1000, null, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("GetIsCallingState", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    if (success.equals("1")) {
                        String Calling = jsonObject.getString("Calling");
                        if (Calling.equals("1")) {

                            int time = 700;
                            countDownTimer(time);


                        } else if (Calling.equals("0")) {
                            // //---------
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void countDownTimer(final int time){

        final CountDownTimer countDownTimer = new CountDownTimer(time, 700) {

            public void onTick(long millisUntilFinished) {
                if(flag==0){
                    SupPage.setBackgroundResource(R.drawable.rsup);
                    flag = 1;
                }else if(flag==1){
                    SupPage.setBackgroundResource(R.drawable.bsup);
                    flag=0;
                }

            }

            public void onFinish() {
                countDownTimer(time);

            }
        };
        countDownTimer.start();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SharedPreferences settings = getSharedPreferences("UserId", 0);
        Crashlytics.setUserIdentifier(settings.getString("userId", ""));
    }


}
