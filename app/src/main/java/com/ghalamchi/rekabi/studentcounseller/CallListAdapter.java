package com.ghalamchi.rekabi.studentcounseller;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ghalamchi.rekabi.studentcounseller.R;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CallListAdapter extends BaseAdapter {

    Context context;
    String userid;
    ArrayList<String> reqId;
    ArrayList<String> SupFullName;
    ArrayList<String> SupTime ;
    ArrayList<String> reqRecoms;
    LayoutInflater inflater;

    public CallListAdapter(Context context,ArrayList<String> name,ArrayList<String> time,ArrayList<String> reqId,ArrayList<String> reqRecoms,String userId){
        this.context = context;
        this.SupFullName = name;
        this.SupTime = time;
        this.reqId = reqId;
        this.userid = userId;
        this.reqRecoms = reqRecoms;
        inflater = LayoutInflater.from(context);
    }



    @Override
    public int getCount() {
        return SupFullName.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if(convertView==null){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_calllist,null);
            viewHolder.SupRecom = convertView.findViewById(R.id.BookRecomNumber);
            viewHolder.SupName = convertView.findViewById(R.id.name);
            viewHolder.SupTime = convertView.findViewById(R.id.time);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.SupName.setText(SupFullName.get(position));
        viewHolder.SupTime.setText(SupTime.get(position));
        viewHolder.SupRecom.setText(reqRecoms.get(position));
        final Typeface tvFont = Typeface.createFromAsset(context.getAssets(), "fonts/B Traffic Bold_p30download.com.ttf");
        viewHolder.SupName.setTypeface(tvFont);
        viewHolder.SupTime.setTypeface(tvFont);
        viewHolder.SupRecom.setTypeface(tvFont);

        return convertView;
    }

    public class ViewHolder{
        TextView SupName,SupTime,SupRecom;
    }




}
