package com.ghalamchi.rekabi.studentcounseller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.ghalamchi.rekabi.studentcounseller.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterActivity extends AppCompatActivity {
    private SpotsDialog alertDialog;
    private Button register;
    private EditText etName, etPhoneNumber;
    private Spinner spSex, spState, spCity, spGroupGrade, spMainGrade;
    private String stateUrl, cityUrl, groupGradeUrl, mainGradeUrl;

    private ArrayList<String> stateNameList = new ArrayList<>();
    private ArrayList<String> stateCodeList = new ArrayList<>();

    private ArrayList<String> cityNameList = new ArrayList<>();
    private ArrayList<String> cityCodeList = new ArrayList<>();

    private ArrayList<String> mainGradeList = new ArrayList<>();
    private ArrayList<String> mainGradeCodeList = new ArrayList<>();
    private ArrayList<String> groupGradeList = new ArrayList<>();
    private ArrayList<String> groupGradeCodeList = new ArrayList<>();

    private ArrayList<String> sexList = new ArrayList<>();

    private String register_success, login_success, userid, registerCode;


    private String userName, userPhone, userSex, userState, userStateCode, userCity, userCityCode, userGradeGroup, userGradeCodeGroup, userGradeMain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Fabric.with(this, new Crashlytics());
        logUser();


        //Logout:
        SharedPreferences settings = getSharedPreferences("UserId",  getApplicationContext().MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();

        //Register:

            etName = findViewById(R.id.etName);
            etPhoneNumber = findViewById(R.id.etPhoneNumber);
            spSex = findViewById(R.id.spSex);
            spState = findViewById(R.id.spState);
            spCity = findViewById(R.id.spCity);
            spMainGrade = findViewById(R.id.spMainGrade);
            spGroupGrade = findViewById(R.id.spGroupGrade);
            register = findViewById(R.id.registerButton);


//            sexList.add("جنسیت");
            sexList.add("دختر");
            sexList.add("پسر");
            ArrayAdapter<String> sexArrayAdapter = new ArrayAdapter<String>(RegisterActivity.this, R.layout.spinner_item, sexList);
            sexArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
            spSex.setAdapter(sexArrayAdapter);


//            stateNameList.add("استان");
//            stateCodeList.add("0");
            cityNameList.add("شهر");
            cityCodeList.add("0");
//            mainGradeList.add("مقطع");
//            mainGradeCodeList.add("0");
            groupGradeList.add("پایه تحصیلی");
            groupGradeCodeList.add("0");

            ArrayAdapter<String> groupAdapter = new ArrayAdapter<String>(RegisterActivity.this, R.layout.spinner_item, groupGradeList);
            groupAdapter.setDropDownViewResource(R.layout.spinner_item);
            spGroupGrade.setAdapter(groupAdapter);

            ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(RegisterActivity.this, R.layout.spinner_item, cityNameList);
            cityAdapter.setDropDownViewResource(R.layout.spinner_item);
            spCity.setAdapter(cityAdapter);

            stateUrl = getResources().getString(R.string.url)+"GetStates";
            cityUrl = getResources().getString(R.string.url)+"GetCities";
            groupGradeUrl = getResources().getString(R.string.urlcup)+"getgroupcodes";
            mainGradeUrl = getResources().getString(R.string.urlcup)+"getmaincodes";

            alertDialog = new SpotsDialog(RegisterActivity.this, R.style.Custom);
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);


            new VolleyPost(RegisterActivity.this, stateUrl, new HashMap<String, String>(), 1000, alertDialog, new VolleyPost.onResponse() {
                @Override
                public void onFinish(String Result) {
                    try {
                        Log.e("res", Result);
                        JSONObject jsonObject = new JSONObject(Result);
                        JSONArray jsonArrayResult = jsonObject.getJSONArray("states");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject statejsonObject = jsonArrayResult.getJSONObject(i);
                            stateNameList.add(statejsonObject.getString("StateName"));
                            stateCodeList.add(statejsonObject.getString("StateCode"));
                        }
                        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(RegisterActivity.this, R.layout.spinner_item, stateNameList);
                        stateAdapter.setDropDownViewResource(R.layout.spinner_item);
                        spState.setAdapter(stateAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });



            new VolleyPost(RegisterActivity.this, mainGradeUrl, new HashMap<String, String>(), 1000, alertDialog, new VolleyPost.onResponse() {
                @Override
                public void onFinish(String Result) {
                    try {
                        Log.e("res", Result);
                        JSONObject jsonObject = new JSONObject(Result);
                        JSONArray jsonArrayResult = jsonObject.getJSONArray("mains");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject mainjsonObject = jsonArrayResult.getJSONObject(i);
                            mainGradeList.add(mainjsonObject.getString("MainName"));
                            mainGradeCodeList.add(mainjsonObject.getString("Id"));
                        }
                        ArrayAdapter<String> groupMainAdapter = new ArrayAdapter<String>(RegisterActivity.this, R.layout.spinner_item, mainGradeList);
                        groupMainAdapter.setDropDownViewResource(R.layout.spinner_item);
                        spMainGrade.setAdapter(groupMainAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });



            spMainGrade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position < 1) {
                        userGradeMain = "";
                    } else {
                        groupGradeList.clear();
//                        groupGradeList.add("پایه تحصیلی");
                        groupGradeCodeList.clear();
//                        groupGradeCodeList.add("0");
                        String ID = mainGradeCodeList.get(position);
                        Log.e("ID", ID);

                        Map<String, String> paramsMain = new HashMap<String, String>();
                        paramsMain.put("mainCode", ID);
                        paramsMain.put("schoolYear", "9798");
                        new VolleyPost(RegisterActivity.this, groupGradeUrl, paramsMain, 1000, alertDialog, new VolleyPost.onResponse() {
                            @Override
                            public void onFinish(String Result) {
                                try {
                                    Log.e("res", Result);
                                    JSONObject jsonObject = new JSONObject(Result);
                                    JSONArray jsonArrayResult = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArrayResult.length(); i++) {
                                        JSONObject mainjsonObject = jsonArrayResult.getJSONObject(i);
                                        groupGradeList.add(mainjsonObject.getString("groupName"));
                                        groupGradeCodeList.add(mainjsonObject.getString("Id"));
                                    }
                                    ArrayAdapter<String> groupGradeAdapter = new ArrayAdapter<String>(RegisterActivity.this, R.layout.spinner_item, groupGradeList);
                                    groupGradeAdapter.setDropDownViewResource(R.layout.spinner_item);
                                    spGroupGrade.setAdapter(groupGradeAdapter);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });


                        userGradeMain = mainGradeList.get(position);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    userGradeMain = "";
                }
            });


            spGroupGrade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                    if (position == 0) {
//                        userGradeGroup = "";
//                    } else {
                        userGradeGroup = groupGradeList.get(position);
                        userGradeCodeGroup = groupGradeCodeList.get(position);
//                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    userGradeGroup = groupGradeList.get(0);
                }
            });

            spSex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                    if (position == 0) {
//                        userSex = "";
//                    } else
                    if(position==0){
                        userSex = "0";
                    } else if(position==1){
                        userSex = "1";
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    userSex = sexList.get(0);
                }
            });


            spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position < 0) {
                        userState = "";
                        userStateCode = "";
                    } else {
                        cityNameList.clear();
                        cityCodeList.clear();
//                        cityNameList.add("شهر");
//                        cityCodeList.add("0");
                        userState = stateNameList.get(position);
                        userStateCode = stateCodeList.get(position);

                        Map<String, String> stateMap = new HashMap<String, String>();
                        stateMap.put("stateCode", userStateCode);
                        stateMap.put("year", "98");

                        new VolleyPost(RegisterActivity.this, cityUrl, stateMap, 1000, alertDialog, new VolleyPost.onResponse() {
                            @Override
                            public void onFinish(String Result) {
                                try {
                                    Log.e("res", Result);
                                    JSONObject jsonObject = new JSONObject(Result);
                                    JSONArray jsonArrayResult = jsonObject.getJSONArray("cities");
                                    for (int i = 0; i < jsonArrayResult.length(); i++) {
                                        JSONObject cityjsonObject = jsonArrayResult.getJSONObject(i);
                                        cityNameList.add(cityjsonObject.getString("CityName"));
                                        cityCodeList.add(cityjsonObject.getString("CityCode"));
                                    }
                                    ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(RegisterActivity.this, R.layout.spinner_item, cityNameList);
                                    cityAdapter.setDropDownViewResource(R.layout.spinner_item);
                                    spCity.setAdapter(cityAdapter);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });


                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    userState = stateNameList.get(0);
                    userStateCode = stateCodeList.get(0);
                }
            });


            spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position< 0) {
                        userCity = "";
                        userCityCode = "";
                    } else {
                        userCity = cityNameList.get(position);
                        userCityCode = cityCodeList.get(position);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    userCity = cityNameList.get(0);
                    userCityCode =cityCodeList.get(0);
                }
            });


            register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String userPhonetext = etPhoneNumber.getText().toString();
                    userName = etName.getText().toString();
                    if (userPhonetext.length() != 11) {
                        etPhoneNumber.setTextColor(getResources().getColor(R.color.Red));
                        userPhone = "";
                    } else if ((userPhonetext.length() == 11)) {
                        etPhoneNumber.setTextColor(getResources().getColor(R.color.black));
                        userPhone = userPhonetext;
                    }

                    if (userName == "" || userPhone == "" || userSex == "" || userState == "" || userStateCode == "" || userCity == "" || userCityCode == "" || userGradeGroup == "" || userGradeMain == "") {
                        Toast.makeText(getApplicationContext(), "لطفا تمامی فیلدها را وارد نمایید!", Toast.LENGTH_LONG).show();
                    } else {

                        Map<String, String> params = new HashMap<String, String>();
                        params.put("mobile", userPhone);
                        params.put("sex", userSex);
                        params.put("fullName", userName);
                        params.put("stateName", userState);
                        params.put("cityName", userCity);
                        params.put("stateCode", userStateCode);
                        params.put("cityCode", userCityCode);
                        params.put("groupcode", userGradeCodeGroup);
                        params.put("groupname", userGradeGroup);

                        String registerUrl = getResources().getString(R.string.url)+"RegisterUser";
                        new VolleyPost(RegisterActivity.this, registerUrl, params, 1000, alertDialog, new VolleyPost.onResponse() {
                            @Override
                            public void onFinish(String Result) {
                                try {
                                    Log.e("register", Result);
                                    JSONObject jsonObject = new JSONObject(Result);
                                    String success = jsonObject.getString("success");
                                    if(success.equals("1")){
                                        userid = jsonObject.getString("userId");
                                        userName = jsonObject.getString("userName");

                                    }
                                    else if(success.equals("0")){
                                        Toast.makeText(getApplicationContext(),"شما قبلا ثبت نام کرده اید!",Toast.LENGTH_LONG).show();
                                        JSONObject user = jsonObject.getJSONObject("user");
                                        userid = user.getString("Id");
                                        userName = user.getString("FullName");
                                        userPhone = user.getString("Mobile");
                                    }

                                    SharedPreferences settings = getSharedPreferences("UserId", getApplicationContext().MODE_PRIVATE);
                                    SharedPreferences.Editor editor = settings.edit();
                                    editor.putString("userName", userName);
                                    editor.putString("phone", userPhone);
                                    editor.putString("userId", userid);
                                    editor.commit();
                                    editor.apply();

                                    Intent intent = new Intent(RegisterActivity.this, SmsActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra("userName", userName);
                                    intent.putExtra("phone", userPhone);
                                    intent.putExtra("userId", userid);
                                    startActivity(intent);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            });
//        }
        }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog!=null) {
            alertDialog.dismiss();
            alertDialog=null;
        }
    }


    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SharedPreferences settings = getSharedPreferences("UserId", getApplicationContext().MODE_PRIVATE);
        Crashlytics.setUserIdentifier(settings.getString("userId", ""));
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    }