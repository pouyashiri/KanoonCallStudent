package com.ghalamchi.rekabi.studentcounseller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.ghalamchi.rekabi.studentcounseller.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FeedbackActivity extends AppCompatActivity {
    private SpotsDialog alertDialog;
    private int star1turn=1,star2turn=1,star3turn=1,star4turn=1,star5turn=1;
    private ImageView star1, star2, star3, star4, star5;
    private TextView bad1, bad2, bad3,good1, good2, good3;
    private Button getScore;
    private SharedPreferences preferences;
    private int Score = 1;
    private int bad1val = 0, bad2val = 0, bad3val = 0,good1val = 0, good2val = 0, good3val = 0;
    private TextView CounsellorName;
    private int bad1turn = 1, bad2turn = 1, bad3turn = 1,good1turn = 1, good2turn = 1, good3turn = 1;
    private ImageView item1, item2, item3,item4, item5, item6;
    private String userid, reqId, FeedbackDone, supporterFullName;
    private LinearLayout lessThanFive,equalFive;

    private LinearLayout b1,b2,b3,g1,g2,g3;

    private ArrayList<String> FeedbackItems = new ArrayList<>();
    private ArrayList<Integer> ItemId = new ArrayList<>();
    private ArrayList<Integer> ItemValue = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Fabric.with(this, new Crashlytics());
        logUser();

        getScore = findViewById(R.id.getScore);
        star1 = findViewById(R.id.star1);
        star2 = findViewById(R.id.star2);
        star3 = findViewById(R.id.star3);
        star4 = findViewById(R.id.star4);
        star5 = findViewById(R.id.star5);

        b1 = findViewById(R.id.b1);b2 = findViewById(R.id.b2);b3 = findViewById(R.id.b3);
        g1 = findViewById(R.id.g1);g2 = findViewById(R.id.g2);g3 = findViewById(R.id.g3);


        bad1 = findViewById(R.id.bad1);bad2 = findViewById(R.id.bad2);bad3 = findViewById(R.id.bad3);
        good1 = findViewById(R.id.good1);good2 = findViewById(R.id.good2);good3 = findViewById(R.id.good3);

        item1 = findViewById(R.id.item1);item2 = findViewById(R.id.item2);item3 = findViewById(R.id.item3);
        item4 = findViewById(R.id.item4);item5 = findViewById(R.id.item5);item6 = findViewById(R.id.item6);

        CounsellorName = findViewById(R.id.CounsellorName);


        lessThanFive = findViewById(R.id.lessThanFive);
        lessThanFive.setVisibility(View.VISIBLE);

        equalFive = findViewById(R.id.equalFive);
        equalFive.setVisibility(View.INVISIBLE);

        item1.setImageResource(R.drawable.gfeed1);
        item2.setImageResource(R.drawable.gfeed2);
        item3.setImageResource(R.drawable.gfeed3);
        item4.setImageResource(R.drawable.gfeed1);
        item5.setImageResource(R.drawable.gfeed2);
        item6.setImageResource(R.drawable.gfeed3);

        star1.setImageDrawable(getResources().getDrawable(R.drawable.yellowstar));
        star2.setImageDrawable(getResources().getDrawable(R.drawable.graystar));
        star3.setImageDrawable(getResources().getDrawable(R.drawable.graystar));
        star4.setImageDrawable(getResources().getDrawable(R.drawable.graystar));
        star5.setImageDrawable(getResources().getDrawable(R.drawable.graystar));


        SharedPreferences preferences = getSharedPreferences("UserId", 0);
        Bundle data = getIntent().getExtras();
        supporterFullName = getIntent().getStringExtra("supporterName");
        reqId = getIntent().getStringExtra("reqId");
        if (reqId == null) {
//
////            supporterFullName = data.getString("supporterName");
//        }else{
            reqId = preferences.getString("reqId", "");

        }


        CounsellorName.setText("مشاور شما: " + supporterFullName);

        getFeedbackItems();


        star1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                star2turn=1;star3turn=1;star4turn=1;star5turn=1;

                star1.setImageDrawable(getResources().getDrawable(R.drawable.yellowstar));
                star2.setImageDrawable(getResources().getDrawable(R.drawable.graystar));
                star3.setImageDrawable(getResources().getDrawable(R.drawable.graystar));
                star4.setImageDrawable(getResources().getDrawable(R.drawable.graystar));
                star5.setImageDrawable(getResources().getDrawable(R.drawable.graystar));
                Score = 1;
                lessThanFive.setVisibility(View.VISIBLE);
                equalFive.setVisibility(View.INVISIBLE);
                if(star1turn==1){
                    bad1turn = 1; bad2turn = 1; bad3turn = 1;good1turn = 1; good2turn = 1; good3turn = 1;
                    bad1val=0;bad2val=0;bad3val=0;good1val=0;good2val=0;good3val=0;
                    item1.setImageResource(R.drawable.gfeed1);
                    item2.setImageResource(R.drawable.gfeed2);
                    item3.setImageResource(R.drawable.gfeed3);
                    item4.setImageResource(R.drawable.gfeed1);
                    item5.setImageResource(R.drawable.gfeed2);
                    item6.setImageResource(R.drawable.gfeed3);
                    star1turn=2;
                }

            }
        });

        star2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                star1turn=1;star3turn=1;star4turn=1;star5turn=1;

                star1.setImageDrawable(getResources().getDrawable(R.drawable.yellowstar));
                star2.setImageDrawable(getResources().getDrawable(R.drawable.yellowstar));
                star3.setImageDrawable(getResources().getDrawable(R.drawable.graystar));
                star4.setImageDrawable(getResources().getDrawable(R.drawable.graystar));
                star5.setImageDrawable(getResources().getDrawable(R.drawable.graystar));
                Score = 2;
                lessThanFive.setVisibility(View.VISIBLE);
                equalFive.setVisibility(View.INVISIBLE);
                if(star2turn==1){
                    bad1turn = 1; bad2turn = 1; bad3turn = 1;good1turn = 1; good2turn = 1; good3turn = 1;
                    bad1val=0;bad2val=0;bad3val=0;good1val=0;good2val=0;good3val=0;
                    item1.setImageResource(R.drawable.gfeed1);
                    item2.setImageResource(R.drawable.gfeed2);
                    item3.setImageResource(R.drawable.gfeed3);
                    item4.setImageResource(R.drawable.gfeed1);
                    item5.setImageResource(R.drawable.gfeed2);
                    item6.setImageResource(R.drawable.gfeed3);
                    star2turn=2;
                }

            }
        });

        star3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                star1turn=1;star2turn=1;star4turn=1;star5turn=1;

                star1.setImageDrawable(getResources().getDrawable(R.drawable.yellowstar));
                star2.setImageDrawable(getResources().getDrawable(R.drawable.yellowstar));
                star3.setImageDrawable(getResources().getDrawable(R.drawable.yellowstar));
                star4.setImageDrawable(getResources().getDrawable(R.drawable.graystar));
                star5.setImageDrawable(getResources().getDrawable(R.drawable.graystar));
                Score = 3;
                lessThanFive.setVisibility(View.VISIBLE);
                equalFive.setVisibility(View.INVISIBLE);
                if(star3turn==1){
                    bad1turn = 1; bad2turn = 1; bad3turn = 1;good1turn = 1; good2turn = 1; good3turn = 1;
                    bad1val=0;bad2val=0;bad3val=0;good1val=0;good2val=0;good3val=0;
                    item1.setImageResource(R.drawable.gfeed1);
                    item2.setImageResource(R.drawable.gfeed2);
                    item3.setImageResource(R.drawable.gfeed3);
                    item4.setImageResource(R.drawable.gfeed1);
                    item5.setImageResource(R.drawable.gfeed2);
                    item6.setImageResource(R.drawable.gfeed3);
                    star3turn=2;
                }

            }
        });

        star4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                star1turn=1;star2turn=1;star3turn=1;star5turn=1;

                star1.setImageDrawable(getResources().getDrawable(R.drawable.yellowstar));
                star2.setImageDrawable(getResources().getDrawable(R.drawable.yellowstar));
                star3.setImageDrawable(getResources().getDrawable(R.drawable.yellowstar));
                star4.setImageDrawable(getResources().getDrawable(R.drawable.yellowstar));
                star5.setImageDrawable(getResources().getDrawable(R.drawable.graystar));
                Score = 4;
                lessThanFive.setVisibility(View.VISIBLE);
                equalFive.setVisibility(View.INVISIBLE);
                if(star4turn==1){
                    bad1turn = 1; bad2turn = 1; bad3turn = 1;good1turn = 1; good2turn = 1; good3turn = 1;
                    bad1val=0;bad2val=0;bad3val=0;good1val=0;good2val=0;good3val=0;
                    item1.setImageResource(R.drawable.gfeed1);
                    item2.setImageResource(R.drawable.gfeed2);
                    item3.setImageResource(R.drawable.gfeed3);
                    item4.setImageResource(R.drawable.gfeed1);
                    item5.setImageResource(R.drawable.gfeed2);
                    item6.setImageResource(R.drawable.gfeed3);
                    star4turn=2;
                }

            }
        });


        star5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                star1turn=1;star2turn=1;star3turn=1;star4turn=1;

                star1.setImageDrawable(getResources().getDrawable(R.drawable.yellowstar));
                star2.setImageDrawable(getResources().getDrawable(R.drawable.yellowstar));
                star3.setImageDrawable(getResources().getDrawable(R.drawable.yellowstar));
                star4.setImageDrawable(getResources().getDrawable(R.drawable.yellowstar));
                star5.setImageDrawable(getResources().getDrawable(R.drawable.yellowstar));
                Score = 5;
                lessThanFive.setVisibility(View.INVISIBLE);
                equalFive.setVisibility(View.VISIBLE);
                if(star5turn==1){
                    bad1turn = 1; bad2turn = 1; bad3turn = 1;good1turn = 1; good2turn = 1; good3turn = 1;
                    bad1val=0;bad2val=0;bad3val=0;good1val=0;good2val=0;good3val=0;
                    item1.setImageResource(R.drawable.gfeed1);
                    item2.setImageResource(R.drawable.gfeed2);
                    item3.setImageResource(R.drawable.gfeed3);
                    item4.setImageResource(R.drawable.gfeed1);
                    item5.setImageResource(R.drawable.gfeed2);
                    item6.setImageResource(R.drawable.gfeed3);
                    star5turn=2;
                }

            }
        });


        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bad1turn == 1) {
                    item1.setImageResource(R.drawable.rfeed1);
                    bad1val = 1;
                    bad1turn = 2;
                } else if (bad1turn == 2) {
                    item1.setImageResource(R.drawable.gfeed1);
                    bad1val = 0;
                    bad1turn = 1;
                }


            }
        });


        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bad2turn == 1) {
                    item2.setImageResource(R.drawable.rfeed2);
                    bad2val = 1;
                    bad2turn = 2;
                } else if (bad2turn == 2) {
                    item2.setImageResource(R.drawable.gfeed2);
                    bad2val = 0;
                    bad2turn = 1;
                }


            }
        });


        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bad3turn == 1) {
                    item3.setImageResource(R.drawable.rfeed3);
                    bad3val = 1;
                    bad3turn = 2;
                } else if (bad3turn == 2) {
                    item3.setImageResource(R.drawable.gfeed3);
                    bad3val = 0;
                    bad3turn = 1;
                }


            }
        });



        g1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (good1turn == 1) {
                    item4.setImageResource(R.drawable.bfeed1);
                    good1val = 1;
                    good1turn = 2;
                } else if (good1turn == 2) {
                    item4.setImageResource(R.drawable.gfeed1);
                    good1val = 0;
                    good1turn = 1;
                }


            }
        });


        g2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (good2turn == 1) {
                    item5.setImageResource(R.drawable.bfeed2);
                    good2val = 1;
                    good2turn = 2;
                } else if (good2turn == 2) {
                    item5.setImageResource(R.drawable.gfeed2);
                    good2val = 0;
                    good2turn = 1;
                }


            }
        });


        g3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (good3turn == 1) {
                    item6.setImageResource(R.drawable.bfeed3);
                    good3val = 1;
                    good3turn = 2;
                } else if (good3turn == 2) {
                    item6.setImageResource(R.drawable.gfeed3);
                    good3val = 0;
                    good3turn = 1;
                }


            }
        });


        FeedbackDone = "0";
        SharedPreferences settings = getSharedPreferences("UserId", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("reqId", reqId);
        editor.putString("FeedbackDone", FeedbackDone);
        editor.commit();
        editor.apply();


        getScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemValue.clear();
                JSONArray arrayid = new JSONArray(ItemId);
                String itemId0 = arrayid.toString();
                String feedbackIds = itemId0.substring(1, itemId0.length() - 1);


                ItemValue.add(Score);
                ItemValue.add(bad1val);
                ItemValue.add(bad2val);
                ItemValue.add(bad3val);

                ItemValue.add(good1val);
                ItemValue.add(good2val);
                ItemValue.add(good3val);
                JSONArray arrayval = new JSONArray(ItemValue);
                String itemValue0 = arrayval.toString();
                String values = itemValue0.substring(1, itemValue0.length() - 1);

                Log.e("feedbackids", feedbackIds);
                Log.e("values", values);
                Log.e("reqId", reqId);

                RegisterFeedbackItem(reqId, feedbackIds, values);
            }
        });
    }


    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    public void getFeedbackItems() {
        FeedbackItems.clear();
        ItemId.clear();

        alertDialog = new SpotsDialog(FeedbackActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> itemParams = new HashMap<String, String>();
        String isUser = "1";
        itemParams.put("isUser", isUser);

        final String feedbackItemUrl = getResources().getString(R.string.url) + "GetFeedBackItems";
        new VolleyPost(FeedbackActivity.this, feedbackItemUrl, itemParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("GetFeedBackItems", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    JSONArray items = jsonObject.getJSONArray("items");
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject jsonObject1 = items.getJSONObject(i);
                        FeedbackItems.add(jsonObject1.getString("Title"));
                        ItemId.add(jsonObject1.getInt("Id"));
                    }

                    bad1.setText(FeedbackItems.get(1));
                    bad2.setText(FeedbackItems.get(2));
                    bad3.setText(FeedbackItems.get(3));

                    good1.setText(FeedbackItems.get(4));
                    good2.setText(FeedbackItems.get(5));
                    good3.setText(FeedbackItems.get(6));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void RegisterFeedbackItem(String reqId, String feedbackIds, String values) {

        alertDialog = new SpotsDialog(FeedbackActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        Map<String, String> FeedbackParam = new HashMap<String, String>();
        FeedbackParam.put("reqId", reqId);
        FeedbackParam.put("feedbackIds", feedbackIds);
        FeedbackParam.put("values", values);

        final String RegisterUserFeedbackURL = getResources().getString(R.string.url) + "RegisterUserFeedback";
        new VolleyPost(FeedbackActivity.this, RegisterUserFeedbackURL, FeedbackParam, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("UsrFeedback = ", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    if (success.equals("1")) {
                        FeedbackDone = "1";
                        String reqIdnext = "";
                        SharedPreferences settings = getSharedPreferences("UserId", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("reqId", reqIdnext);
                        editor.putString("FeedbackDone", FeedbackDone);
                        editor.commit();
                        editor.apply();
                        Toast.makeText(FeedbackActivity.this, "نظرسنجی شما به ثبت رسید.", Toast.LENGTH_LONG).show();

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(FeedbackActivity.this, HomeActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                            }
                        }, 2000);
                    } else {
                        Toast.makeText(FeedbackActivity.this, "دوباره تلاش کنید.", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SharedPreferences settings = getSharedPreferences("UserId", 0);
        Crashlytics.setUserIdentifier(settings.getString("userId", ""));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
