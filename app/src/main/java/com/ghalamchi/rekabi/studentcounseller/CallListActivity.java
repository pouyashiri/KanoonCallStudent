package com.ghalamchi.rekabi.studentcounseller;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.ghalamchi.rekabi.studentcounseller.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CallListActivity extends AppCompatActivity {

    private SpotsDialog alertDialog;
    private Dialog alertdialog_exit,reqdialog;
    private Button exit,CallListPage, HomePage,BookListPage,SupPage;
    private String userId,userName;
    private TextView new_count;
    private ArrayList<String> SupFullNames = new ArrayList<>();
    private ArrayList<String> SupTimes = new ArrayList<>();
    private ArrayList<String> reqIds = new ArrayList<>();
    private ArrayList<String> BookNumbers = new ArrayList<>();
    private ArrayList<String> reqBookNames = new ArrayList<>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_list);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Fabric.with(this, new Crashlytics());
        logUser();
        exit = findViewById(R.id.exit);
        CallListPage = findViewById(R.id.CallListPage);
        HomePage = findViewById(R.id.HomePage);
        SupPage = findViewById(R.id.SupPage);
        BookListPage = findViewById(R.id.BookListPage);
        new_count = findViewById(R.id.new_count);
        new_count.setVisibility(View.INVISIBLE);



        userId = getIntent().getStringExtra("userId");
        userName = getIntent().getStringExtra("userName");
        Log.e("userId in call list::",userId+"--");

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Alertdialog_exit();
            }
        });

        CallListPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // //Im here
            }
        });

        BookListPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CallListActivity.this, BookListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("userId", userId);
                intent.putExtra("userName", userName);
                startActivity(intent);
            }
        });

        HomePage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CallListActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("userId", userId);
                intent.putExtra("userName", userName);
                startActivity(intent);
            }
        });

        SupPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences settings = getSharedPreferences("UserId", 0);
                if (settings.contains("reqId")){
                    String reqId = settings.getString("reqId","");
                    if(reqId==null || reqId.equals("")){
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("supFullName", "");
                        editor.putString("supField", "");
                        editor.putString("supUniversity", "");
                        editor.putString("supExamRank", "");
                        editor.commit();
                        editor.apply();
                        Toast.makeText(CallListActivity.this,"درخواست مشاوره ای یافت نشد.",Toast.LENGTH_LONG).show();
                    }else{
                        CheckSupporter(reqId);
                    }
                }else{
                    Toast.makeText(CallListActivity.this,"درخواست مشاوره ای یافت نشد.",Toast.LENGTH_LONG).show();
                }
            }
        });




    }


    @Override
    protected void onResume() {
        super.onResume();
        ////////// create Call List

        getCallList(userId);
        GetNewRecomState(userId);
    }

    public void getCallList(String userId0){

        SupTimes.clear();
        reqIds.clear();
        BookNumbers.clear();
        SupFullNames.clear();

        final String userId = userId0;
        alertDialog = null;
        Map<String, String> CallListParams = new HashMap<String, String>();
        CallListParams.put("userId", userId);
        final String CallListUrl = getResources().getString(R.string.url)+"GetAllUserCalls";
        new VolleyPost(CallListActivity.this, CallListUrl, CallListParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("CallList: ", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    JSONArray jsonArrayResult = jsonObject.getJSONArray("calls");
                    for (int i = 0; i < jsonArrayResult.length(); i++) {
                        JSONObject calljsonObject = jsonArrayResult.getJSONObject(i);
                        SupTimes.add(calljsonObject.getString("RequestDate"));
                        reqIds.add(calljsonObject.getString("RequestId"));
                        BookNumbers.add(calljsonObject.getString("BookNumber"));
//                        JSONObject supporter = calljsonObject.getJSONObject("Supporter");
                        SupFullNames.add(calljsonObject.getString("Supporter"));

                    }
                    Log.e("userId = ", userId+"--");
                    Log.e("SupFullNames = ",SupFullNames.toString()+"-");
                    Log.e("SupTimes = ",SupTimes.toString()+"-");
                    Log.e("reqIds = ",reqIds.toString()+"-");
                    Log.e("BookNumbers = ",BookNumbers.toString()+"-");

                    final ListView listView = findViewById(R.id.CallList);
                    CallListAdapter callListAdapter = new CallListAdapter(getApplicationContext(),SupFullNames,SupTimes,reqIds,BookNumbers,userId);
                    listView.setAdapter(callListAdapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            if(!BookNumbers.get(position).equals("0")){
                                String thisReqId = reqIds.get(position);
                                GetRequestRecom(thisReqId);
                            }else{
                                Toast.makeText(getApplicationContext(),"پیشنهاد کتابی موجود نیست.",Toast.LENGTH_LONG).show();
                            }

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void Alertdialog_exit() {
        alertdialog_exit = new Dialog(CallListActivity.this);
        alertdialog_exit.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertdialog_exit.setContentView(R.layout.alert_style_exit);
        Button yesExit = (Button) alertdialog_exit.findViewById(R.id.yesExit);
        yesExit.setEnabled(true);
        Button noExit = (Button) alertdialog_exit.findViewById(R.id.noExit);
        noExit.setEnabled(true);

        yesExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences settings = getSharedPreferences("UserId", getApplicationContext().MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.clear();
                editor.commit();
                Intent intent = new Intent(CallListActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                alertdialog_exit.cancel();
            }
        });
        noExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertdialog_exit.cancel();
            }
        });
        alertdialog_exit.setCancelable(true);
        alertdialog_exit.show();
    }

    public void Alertdialog_ReqRecom() {
        reqdialog = new Dialog(CallListActivity.this);
        reqdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        reqdialog.setContentView(R.layout.alert_reqrecom);

        ListView listViewBookNames = reqdialog.findViewById(R.id.reqRecomList);
        SlotTimeAdapter slotTimeAdapter = new SlotTimeAdapter(reqdialog.getContext(), reqBookNames);
        listViewBookNames.setAdapter(slotTimeAdapter);


        reqdialog.setCancelable(true);
        reqdialog.show();
    }


    public void GetRequestRecom(String reqId){
        reqBookNames.clear();

        alertDialog = null;
        Map<String, String> reqRecomParams = new HashMap<String, String>();
        reqRecomParams.put("reqId", reqId);
        final String reqRecomUrl = getResources().getString(R.string.url)+"GetRequestRecommendation";
        new VolleyPost(CallListActivity.this, reqRecomUrl, reqRecomParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("thisReqRecom: ", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    JSONArray jsonArrayResult = jsonObject.getJSONArray("book");
                    for (int i = 0; i < jsonArrayResult.length(); i++) {
                        JSONObject bookjsonObject = jsonArrayResult.getJSONObject(i);
                        reqBookNames.add(bookjsonObject.getString("BookTitle"));
                    }
                    Alertdialog_ReqRecom();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void GetNewRecomState(String userId){
        alertDialog = new SpotsDialog(CallListActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> RemoveRequestParams = new HashMap<String, String>();
        RemoveRequestParams.put("userId", userId);

        final String RemoveRequestUrl = getResources().getString(R.string.url) + "GetNewRecomState";
        new VolleyPost(CallListActivity.this, RemoveRequestUrl, RemoveRequestParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("GetNewRecomState", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    int count = jsonObject.getInt("count");
                    if(count==0){
                        //// Invisible the new circle
                        new_count.setVisibility(View.INVISIBLE);
                    }else{
                        new_count.setVisibility(View.VISIBLE);
                        String countStr = String.valueOf(count);
                        new_count.setText(countStr);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void CheckSupporter(String reqId0){

        alertDialog = new SpotsDialog(CallListActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> GetIsCallingStateParams = new HashMap<String, String>();
        GetIsCallingStateParams.put("reqId", reqId0);

        final String GetIsCallingStateUrl = getResources().getString(R.string.url) + "GetIsCallingState";
        new VolleyPost(CallListActivity.this, GetIsCallingStateUrl,GetIsCallingStateParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("GetIsCallingState", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    if (success.equals("1")) {
                        String Calling = jsonObject.getString("Calling");
                        if(Calling.equals("1")){
                            JSONObject supporter = jsonObject.getJSONObject("supporter");
                            String supFullName = supporter.getString("FullName");
                            String supField = supporter.getString("Field");
                            String supUniversity = supporter.getString("University");
                            String supExamRank = supporter.getString("ExamRank");
                            SharedPreferences settings = getSharedPreferences("UserId",0);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("supFullName", supFullName);
                            editor.putString("supField", supField);
                            editor.putString("supUniversity", supUniversity);
                            editor.putString("supExamRank", supExamRank);
                            editor.commit();
                            editor.apply();
                            Intent intent= new Intent(CallListActivity.this,SupporterInfoActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }else if(Calling.equals("0")){
                            SharedPreferences settings = getSharedPreferences("UserId",0);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("supFullName", "");
                            editor.putString("supField", "");
                            editor.putString("supUniversity", "");
                            editor.putString("supExamRank", "");
                            editor.commit();
                            editor.apply();
                            Toast.makeText(CallListActivity.this,"تا کنون مشاوری به درخواست شما پاسخ نداده است. لطفا منتظر بمانید!",Toast.LENGTH_LONG).show();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CallListActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("userId", userId);
        intent.putExtra("userName", userName);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SharedPreferences settings = getSharedPreferences("UserId", getApplicationContext().MODE_PRIVATE);
        Crashlytics.setUserIdentifier(settings.getString("userId", ""));
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
