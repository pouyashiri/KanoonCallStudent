package com.ghalamchi.rekabi.studentcounseller;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ghalamchi.rekabi.studentcounseller.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class Splash extends AppCompatActivity {

    private SpotsDialog alertDialog;


    @NeedsPermission({android.Manifest.permission.READ_PHONE_STATE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE,android.Manifest.permission.READ_EXTERNAL_STORAGE})
    void checkPermission() {

        start();
        // allow Permission : Code here

    }

    @OnPermissionDenied({android.Manifest.permission.READ_PHONE_STATE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE,android.Manifest.permission.READ_EXTERNAL_STORAGE})
    void showDeniedForCamera() {
        // don't allow Permission : Code here
        Toast.makeText(this, "دسترسی به اطلاعات تماس الزامی می باشد!", Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        SplashPermissionsDispatcher.onRequestPermissionsResult(Splash.this, requestCode, grantResults);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        SplashPermissionsDispatcher.checkPermissionWithPermissionCheck(Splash.this);

//
//        start();
    }

    public void Alertdialog_update(final String newUrl) {
        final Dialog My_alertdialog = new Dialog(Splash.this);
        My_alertdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        My_alertdialog.setContentView(R.layout.alert_forceupdate);
        Button ok = (Button) My_alertdialog.findViewById(R.id.update);
        ok.setEnabled(true);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(newUrl));
                startActivity(intent);
//                finish();
                registerReceiver(attachmentDownloadCompleteReceive, new IntentFilter(
                        DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                final DownloadManager.Request request = new DownloadManager.Request(Uri.parse(newUrl));
                Log.e("MimeType", new DownloadAttachment().getMimeType(Uri.parse(newUrl).toString()));
                request.setMimeType(new DownloadAttachment().getMimeType(Uri.parse(newUrl).toString()));
//                request.setDescription("Some descrition");
                request.setTitle("اپلیکیشن مشاوره");
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "studentCounsellor" + ".apk");
                final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                try {
                    final long downloadId = manager.enqueue(request);

                } catch (Exception e) {
                    Log.e("dl err", e + "");
                }
                My_alertdialog.dismiss();
                My_alertdialog.cancel();
//                AppUpdater appUpdater = new AppUpdater(this);
//                appUpdater.start();

            }
        });

        My_alertdialog.setCancelable(false);
        My_alertdialog.show();

    }
    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    public void start() {

        PackageManager manager = this.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(this.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        final int versionCode = info.versionCode;

        String prefVersion;

        SharedPreferences settings = getSharedPreferences("UserId", 0);
        if (settings.contains("Version")) {
            prefVersion = settings.getString("Version", "0");
        } else {
            prefVersion = "1";
        }

        final String resVersion = versionCode + "";


        if (!resVersion.equals(prefVersion)) {

            SharedPreferences.Editor editor = settings.edit();
            editor.clear();
            editor.commit();
            editor.apply();
        }


        SharedPreferences.Editor editor = settings.edit();
        editor.putString("Version", resVersion);
        editor.commit();
        editor.apply();

        //Get new Version from web service
        Map<String, String> params = new HashMap<String, String>();
        params.put("appId", "2");
        String url = this.getString(R.string.url) + "GetVersion";
        alertDialog = new SpotsDialog(Splash.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        new VolleyPost(Splash.this, url, params, 1000, null, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    JSONObject jsonObject0 = new JSONObject(Result);
                    JSONObject jsonObject = jsonObject0.getJSONObject("Application");
                    String newVersion = jsonObject.getString("Versian");
                    final String newUrl = jsonObject.getString("AppUrl");
                    int dbVersionInt = Integer.parseInt(newVersion);
                    int verionInt = versionCode;
                    Log.e("versionInt", newVersion);

                    Log.e("---pppref Version:", verionInt + "------------");
                    Log.e("---rrrres Version:", dbVersionInt + "------------");

                    if (verionInt < dbVersionInt) {

                        Alertdialog_update(newUrl);

                    } else {
//                    goto login
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //TODO change the intent second activity to login activity
//                                Intent intent = new Intent(SplashActivity.this, SmsActivity.class).putExtra("version",version);
                                Intent intent = new Intent(Splash.this, LoginActivity.class);
//                                intent.putExtra("version", version);
                                startActivity(intent);
                                finish();
                            }
                        }, 2000);
                    }
                } catch (JSONException js) {
                    Log.e("checkVersionError", js + "");
                }
            }
        });


    }

    BroadcastReceiver attachmentDownloadCompleteReceive = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                long downloadId = intent.getLongExtra(
                        DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                new DownloadAttachment(context, downloadId);
            }
        }
    };
}
