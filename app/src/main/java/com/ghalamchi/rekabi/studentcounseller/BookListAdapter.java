package com.ghalamchi.rekabi.studentcounseller;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ghalamchi.rekabi.studentcounseller.R;

import java.util.ArrayList;

public class BookListAdapter extends BaseAdapter {

    Context context;

    ArrayList<String> SupporterNames;
    ArrayList<String> BookNames ;
    ArrayList<String> BookPrices;
    LayoutInflater inflater;

    public BookListAdapter(Context context,ArrayList<String> SupporterNames,ArrayList<String> BookNames,ArrayList<String> BookPrices){
        this.context = context;
        this.SupporterNames = SupporterNames;
        this.BookNames = BookNames;
        this.BookPrices = BookPrices;
        inflater = LayoutInflater.from(context);
    }



    @Override
    public int getCount() {
        return SupporterNames.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if(convertView==null){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_bookrecom,null);
            viewHolder.SupName = convertView.findViewById(R.id.supName);
            viewHolder.BookName = convertView.findViewById(R.id.nameBook);
            viewHolder.BookPrice = convertView.findViewById(R.id.priceBook);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.SupName.setText("مشاور: "+SupporterNames.get(position));
        viewHolder.BookName.setText("کتاب: "+BookNames.get(position));
        viewHolder.BookPrice.setText("قیمت: "+BookPrices.get(position));
        final Typeface tvFont = Typeface.createFromAsset(context.getAssets(), "fonts/B Traffic Bold_p30download.com.ttf");
        viewHolder.SupName.setTypeface(tvFont);
        viewHolder.BookName.setTypeface(tvFont);
        viewHolder.BookPrice.setTypeface(tvFont);

        return convertView;
    }

    public class ViewHolder{
        TextView SupName,BookName,BookPrice;
    }
}
