package com.ghalamchi.rekabi.studentcounseller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.ghalamchi.rekabi.studentcounseller.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SmsActivity extends AppCompatActivity {

    private EditText etcode;
    private TextView milisecText;
    private Button LoginButton, reRequest;
    private ProgressBar progress120;
    private String enterCode, Code, userid, userPhone,userName;
    private String registerCode;
    private SpotsDialog alertDialog;
//    private SharedPreferences preferences;
    private RelativeLayout prog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smscode);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Fabric.with(this, new Crashlytics());
        logUser();

        SharedPreferences preferences = getSharedPreferences("UserId",getApplicationContext().MODE_PRIVATE);
        Log.e("im in Log in ", "Activity");

        progress120 = findViewById(R.id.progress120);
        etcode = findViewById(R.id.code);
        milisecText = findViewById(R.id.milisecText);
        LoginButton = findViewById(R.id.LoginButton);
        reRequest = findViewById(R.id.reRequest);
        prog = findViewById(R.id.prog);


        Bundle data = getIntent().getExtras();
        if (data != null) {
            userName = data.getString("userName");
            userid = data.getString("userId");
            userPhone = data.getString("phone");
        }else{
            if(preferences.contains("userName")){
                userName = preferences.getString("userName", "");
            }
            userid = preferences.getString("userId","");
            userPhone = preferences.getString("phone","");
        }

        // SEND SMS to USER
//        userPhone = "09128651526";
        sendSMS(userPhone);

        final CountDownTimer countDownTimer = new CountDownTimer(120000, 1000) {


            public void onTick(long millisUntilFinished) {
                prog.setVisibility(View.VISIBLE);
                milisecText.setText("" + millisUntilFinished / 1000);
                progress120.setVisibility(View.VISIBLE);
            }

            public void onFinish() {
                reRequest.setVisibility(View.VISIBLE);
                LoginButton.setVisibility(View.INVISIBLE);
                progress120.clearAnimation();
                milisecText.setText("");
                prog.setVisibility(View.INVISIBLE);


            }
        };

        countDownTimer.start();


            reRequest.setVisibility(View.INVISIBLE);
            LoginButton.setVisibility(View.VISIBLE);

            LoginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    enterCode = etcode.getText().toString();
                    Log.e("code man",enterCode);
                    Log.e("code asli",Code);

                    if (enterCode.equals(Code)) {
                        ActivateUser();
                    } else {
                        countDownTimer.cancel();
                        Toast.makeText(getApplicationContext(), "کد وارد شده معتبر نمی باشد.", Toast.LENGTH_LONG).show();
                        LoginButton.setVisibility(View.INVISIBLE);
                        reRequest.setVisibility(View.VISIBLE);
                        progress120.clearAnimation();
                        milisecText.setText("");
                        prog.setVisibility(View.INVISIBLE);
                        etcode.setText("");
                    }
                }
            });

            reRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    countDownTimer.cancel();
                    reRequest.setVisibility(View.INVISIBLE);
                    LoginButton.setVisibility(View.VISIBLE);
                    progress120.clearAnimation();
                    milisecText.setText("");
                    prog.setVisibility(View.INVISIBLE);

                    alertDialog = new SpotsDialog(SmsActivity.this, R.style.Custom);
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    Map<String, String> requestsmsParams = new HashMap<String, String>();
                    requestsmsParams.put("mobile", userPhone);

                    final String requestsmsUrl = getResources().getString(R.string.urlcommon)+"RequestSms";
                    new VolleyPost(SmsActivity.this, requestsmsUrl, requestsmsParams, 1000, alertDialog, new VolleyPost.onResponse() {
                        @Override
                        public void onFinish(String Result) {
                            try {
                                Log.e("requestSms", Result);
                                JSONObject jsonObject = new JSONObject(Result);
                                registerCode = jsonObject.getString("code");
                                Code = registerCode+"";
                                reRequest.setVisibility(View.INVISIBLE);
                                LoginButton.setVisibility(View.VISIBLE);
                                countDownTimer.start();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });


                }
            });
        }


    public void sendSMS(String phone){

        alertDialog = new SpotsDialog(SmsActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        Map<String, String> requestsmsParams = new HashMap<String, String>();
        requestsmsParams.put("mobile", phone);

        final String requestsmsUrl = getResources().getString(R.string.urlcommon)+"RequestSms";
        new VolleyPost(SmsActivity.this, requestsmsUrl, requestsmsParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("requestSms", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    registerCode = jsonObject.getString("code");
                    Code = registerCode;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    public void ActivateUser(){
        alertDialog = new SpotsDialog(SmsActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        Map<String, String> ActivateUserParams = new HashMap<String, String>();
        ActivateUserParams.put("userId", userid);

        final String ActivateUserUrl = getResources().getString(R.string.url)+"ActivateUser";
        new VolleyPost(SmsActivity.this, ActivateUserUrl, ActivateUserParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("ActivateUser", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    if(success.equals("1")){

                        SharedPreferences settings = getSharedPreferences("UserId", getApplicationContext().MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("registerCode", Code);
                        editor.putString("phone", userPhone);
                        editor.putString("userId", userid);
                        editor.putString("userName", userName);
                        editor.commit();
                        editor.apply();


                        Intent intent = new Intent(SmsActivity.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.putExtra("registerCode", Code);
                        intent.putExtra("phone", userPhone);
                        intent.putExtra("userId", userid);
                        intent.putExtra("userName", userName);
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"ورود شما با مشکل مواجه شد. لطفا مجددا تلاش کنید!",Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog!=null) {
            alertDialog.dismiss();
            alertDialog=null;
        }
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SharedPreferences settings = getSharedPreferences("UserId", getApplicationContext().MODE_PRIVATE);
        Crashlytics.setUserIdentifier(settings.getString("userId", ""));
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
