package com.ghalamchi.rekabi.studentcounseller;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.ghalamchi.rekabi.studentcounseller.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BookListActivity extends AppCompatActivity {

    private SpotsDialog alertDialog;
    private Dialog alertdialog_exit;
    private Button exit,CallListPage, HomePage,BookListPage,SupPage;
    private String userId,userName;
    private ArrayList<String> SupFullNames = new ArrayList<>();
    private ArrayList<String> BookNames = new ArrayList<>();
    private ArrayList<String> BookPrices = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_list);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Fabric.with(this, new Crashlytics());
        logUser();
        exit = findViewById(R.id.exit);
        CallListPage = findViewById(R.id.CallListPage);
        HomePage = findViewById(R.id.HomePage);
        SupPage = findViewById(R.id.SupPage);
        BookListPage = findViewById(R.id.BookListPage);




        userId = getIntent().getStringExtra("userId");
        userName = getIntent().getStringExtra("userName");
        Log.e("userId in call list::",userId+"--");

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Alertdialog_exit();
            }
        });

        BookListPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // //Im here
            }
        });

        CallListPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BookListActivity.this, CallListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("userId", userId);
                intent.putExtra("userName", userName);
                startActivity(intent);
            }
        });

        HomePage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BookListActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("userId", userId);
                intent.putExtra("userName", userName);
                startActivity(intent);
            }
        });

        SupPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences settings = getSharedPreferences("UserId", 0);
                if (settings.contains("reqId")){
                    String reqId = settings.getString("reqId","");
                    if(reqId==null || reqId.equals("")){
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("supFullName", "");
                        editor.putString("supField", "");
                        editor.putString("supUniversity", "");
                        editor.putString("supExamRank", "");
                        editor.commit();
                        editor.apply();
                        Toast.makeText(BookListActivity.this,"درخواست مشاوره ای یافت نشد.",Toast.LENGTH_LONG).show();
                    }else{
                        CheckSupporter(reqId);
                    }
                }else{
                    Toast.makeText(BookListActivity.this,"درخواست مشاوره ای یافت نشد.",Toast.LENGTH_LONG).show();
                }
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        /////// create Recommendation List
        getRecomList(userId);
        SetRecomReaded(userId);

    }

    public void getRecomList(String userId){

        SpotsDialog alertDialog = null;
        Map<String, String> CallListParams = new HashMap<String, String>();
        CallListParams.put("userId", userId);
        final String CallListUrl = getResources().getString(R.string.url)+"GetAllRecommendations";
        new VolleyPost(BookListActivity.this, CallListUrl, CallListParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("RecomList: ", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    JSONArray jsonArrayResult = jsonObject.getJSONArray("pendingRecoms");
                    for (int i = 0; i < jsonArrayResult.length(); i++) {
                        JSONObject recomjsonObject = jsonArrayResult.getJSONObject(i);
                        ///////////////// check output of GetAllRecommendations
                        SupFullNames.add(recomjsonObject.getString("FullName"));
                        BookNames.add(recomjsonObject.getString("BookTitle"));
                        BookPrices.add(recomjsonObject.getString("BookPrice"));

                    }
                    final ListView listView = findViewById(R.id.bookList);
                    BookListAdapter bookListAdapter = new BookListAdapter(BookListActivity.this,SupFullNames,BookNames,BookPrices);
                    listView.setAdapter(bookListAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void SetRecomReaded(String userId){
        alertDialog = new SpotsDialog(BookListActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> RemoveRequestParams = new HashMap<String, String>();
        RemoveRequestParams.put("userId", userId);

        final String RemoveRequestUrl = getResources().getString(R.string.url) + "SetRecomReaded";
        new VolleyPost(BookListActivity.this, RemoveRequestUrl, RemoveRequestParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("SetRecomReaded", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    int success = jsonObject.getInt("success");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void CheckSupporter(String reqId0){

        alertDialog = new SpotsDialog(BookListActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> GetIsCallingStateParams = new HashMap<String, String>();
        GetIsCallingStateParams.put("reqId", reqId0);

        final String GetIsCallingStateUrl = getResources().getString(R.string.url) + "GetIsCallingState";
        new VolleyPost(BookListActivity.this, GetIsCallingStateUrl,GetIsCallingStateParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("GetIsCallingState", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    if (success.equals("1")) {
                        String Calling = jsonObject.getString("Calling");
                        if(Calling.equals("1")){
                            JSONObject supporter = jsonObject.getJSONObject("supporter");
                            String supFullName = supporter.getString("FullName");
                            String supField = supporter.getString("Field");
                            String supUniversity = supporter.getString("University");
                            String supExamRank = supporter.getString("ExamRank");
                            SharedPreferences settings = getSharedPreferences("UserId",0);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("supFullName", supFullName);
                            editor.putString("supField", supField);
                            editor.putString("supUniversity", supUniversity);
                            editor.putString("supExamRank", supExamRank);
                            editor.commit();
                            editor.apply();
                            Intent intent= new Intent(BookListActivity.this,SupporterInfoActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }else if(Calling.equals("0")){
                            SharedPreferences settings = getSharedPreferences("UserId",0);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("supFullName", "");
                            editor.putString("supField", "");
                            editor.putString("supUniversity", "");
                            editor.putString("supExamRank", "");
                            editor.commit();
                            editor.apply();
                            Toast.makeText(BookListActivity.this,"تا کنون مشاوری به درخواست شما پاسخ نداده است. لطفا منتظر بمانید!",Toast.LENGTH_LONG).show();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void Alertdialog_exit() {
        alertdialog_exit = new Dialog(BookListActivity.this);
        alertdialog_exit.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertdialog_exit.setContentView(R.layout.alert_style_exit);
        Button yesExit = (Button) alertdialog_exit.findViewById(R.id.yesExit);
        yesExit.setEnabled(true);
        Button noExit = (Button) alertdialog_exit.findViewById(R.id.noExit);
        noExit.setEnabled(true);

        yesExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences settings = getSharedPreferences("UserId", getApplicationContext().MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.clear();
                editor.commit();
                Intent intent = new Intent(BookListActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                alertdialog_exit.cancel();
            }
        });
        noExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertdialog_exit.cancel();
            }
        });
        alertdialog_exit.setCancelable(true);
        alertdialog_exit.show();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(BookListActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("userId", userId);
        intent.putExtra("userName", userName);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SharedPreferences settings = getSharedPreferences("UserId", getApplicationContext().MODE_PRIVATE);
        Crashlytics.setUserIdentifier(settings.getString("userId", ""));
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
