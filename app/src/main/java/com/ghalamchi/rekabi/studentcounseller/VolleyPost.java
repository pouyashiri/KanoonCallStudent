package com.ghalamchi.rekabi.studentcounseller;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by NegarAkbarzade on 7/26/2018.
 */

public class VolleyPost {
    //    ProgressDialog progDiag;
    Context context;

    public VolleyPost(final Context con, final String url, final Map<String, String> params, final int reqTimeOutValue, final AlertDialog alertDialog, final onResponse delegate) {
        this.delegate = delegate;
        this.context = con;
        ReqTimeOutValue = reqTimeOutValue * 2;
        if (ReqTimeOutValue > 63000)
            ReqTimeOutValue = 2000;
//        final ProgressDialog progDiag = new ProgressDialog(con);
        if(!((Activity) context).isFinishing() && alertDialog != null){

                alertDialog.show();




        }

//        if (showPg)
//            progDiag.show();


        Response.Listener<String> l = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if(!((Activity) context).isFinishing() && alertDialog != null)
                    alertDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e("Response:",response);
                    if (jsonObject.has("Status")) {
                        int status = jsonObject.getInt("Status");
                        if (status < 1) {
                            String message = jsonObject.getString("Message");
                            Log.e("Error Message:", message);
//                            Toast.makeText(context, "Error: " + message, Toast.LENGTH_LONG).show();
                        } else {

                            Object data = jsonObject.get("Data");
                            delegate.onFinish(data.toString());

                        }
                    }
                    else
                        delegate.onFinish(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        };
        StringRequest strReq = new StringRequest(Request.Method.POST, url, l, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(!((Activity) context).isFinishing() && alertDialog != null)
                    alertDialog.dismiss();
                Log.e("VolleyError at url='",url + "'. Reason='" + error + "'.");
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    //TODO
                } else if (error instanceof AuthFailureError) {
                    //TODO
                } else if (error instanceof ServerError) {
                    //TODO
                } else if (error instanceof NetworkError) {
                    //TODO
                } else if (error instanceof ParseError) {
                    //TODO
                }
//                if (showPg)
//                    progDiag.dismiss();
//                pg.dismiss();
                if (error instanceof NoConnectionError)
                    Toast.makeText(con, "لطفا از اتصال اینترنت اطمینان حاصل کنید.", Toast.LENGTH_SHORT).show();
                else
//                    Toast.makeText(con, "دوباره تلاش کنید.", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        new VolleyPost(con, url, params, ReqTimeOutValue,alertDialog, delegate);
                    }
                }, ReqTimeOutValue);

            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                }

                return volleyError;
            }


        };
//        strReq.setRetryPolicy(new DefaultRetryPolicy(
//                10000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Initializer apcont = Initializer.getInstance();

        RequestQueue queue = apcont.getRequestQueue();
        queue.add(strReq);

    }

    public interface onResponse {
        void onFinish(String Result);
    }

    public onResponse delegate = null;

    public Map<String, String> params;
    public int ReqTimeOutValue = 2000;


}
